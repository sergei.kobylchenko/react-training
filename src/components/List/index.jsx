import React, { Component } from 'react';
import cx from 'classnames'

import styles from './list.module.css';

class List extends Component {
  state = {
    loading: null,
  };

  componentDidMount() {
    this.setState({ loading: true });
    this.props.load()
      .then(() => this.setState({ loading: false }));
  }

  render() {
    const { data, clickHandler, selected } = this.props;
    const { loading } = this.state;

    return (
      <div>
        {loading && <h1 className={styles.title}>Loaddddinnggg list....</h1>}
        {!loading && (
        <>
          <h1 className={styles.title}>List of files</h1>
          <ul className={styles.list}>
            {data.map(item => (
              <li 
                key={item}
                className={cx(styles.listItem, {[styles.listItemSelected]: selected === item})}
                onClick={() => clickHandler(item)}
              >
                {item}
              </li>
            ))}
          </ul>
        </>)}
      </div>
    );
  }
}

export default List;
