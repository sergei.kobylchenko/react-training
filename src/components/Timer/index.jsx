import React, { Component } from 'react';
import { func, bool, number, object } from 'prop-types';
import cx from 'classnames';

import processValues from 'utils';
import { MINUTE, HOUR } from './constants';
import styles from './timer.module.css';

class Timer extends Component {
  constructor(props) {
    super(props);
    this.audio = new Audio('/sounds/sound.mp3');

    if (props.time) {
      const { min = 0, sec = 0 } = props.time;
      this.state.time = min * MINUTE + sec;
    }
  }

  state = {
    time: null,
    isRunning: false,
    isAlarmed: false,
    isFinished: false,
  };

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  start = () => {
    const { time } = this.state;

    if (!time) {
      return;
    }

    this.setState({ isRunning: true });
    this.tick();
    this.intervalID = setInterval(this.tick, 1000);
  };

  pause = () => {
    clearInterval(this.intervalID);
    this.setState({ isRunning: false });
  };

  reset = () => {
    clearInterval(this.intervalID);
    this.audio.pause();
    this.audio.currentTime = 0;
    this.setState({
      time: 0,
      isRunning: false,
      isFinished: false,
      isAlarmed: false,
    });
  };

  incrementMinutes = () => {
    const time = this.state.time + MINUTE;
    this.setState({ time });
  };

  decrementMinutes = () => {
    const decrementValue = this.state.time - MINUTE;
    const time = decrementValue < 0 ? HOUR + decrementValue : decrementValue;
    this.setState({ time });
  };

  tick = () => {
    const { time } = this.state;

    let newState = { time: time - 1 };

    if (time === 0) {
      newState = {
        isFinished: true,
        isRunning: false,
        isAlarmed: false,
      };
      clearInterval(this.intervalID);
      this.audio.play();
      this.props.callback();
    }

    if (time <= this.props.endingTime && time) {
      newState = { ...newState, isAlarmed: true };
    }

    this.setState(newState);
  };

  render() {
    const { controls } = this.props;
    const { 
      time,
      isRunning,
      isFinished,
      isAlarmed,
    } = this.state;

    return (
      <div className={cx(styles.container, { [styles.finish]: isFinished })}>
        <h1 className={styles.title}>Timer</h1>

        <div className={styles.time}>
          <div className={styles.minute}>
            {controls && !isRunning && (
              <>
                <span onClick={this.incrementMinutes} className={styles.up} />
                <span onClick={this.decrementMinutes} className={styles.down} />
              </>
            )}
            <span className={styles.minutes}>{processValues.getMinutes(time)}</span>
          </div>
          <span>:</span>
          <div className={styles.seconds}>
            <span className={cx({ [styles.secondsAlarm]: isAlarmed })}>
              {processValues.getSeconds(time)}
            </span>
          </div>
        </div>

        {controls && (
          <>
            {!isRunning && !isFinished && (
              <button onClick={this.start} className={cx(styles.btn, styles.btnLaunch)}>
                Launch timer!
              </button>
            )}
            {isRunning && (
              <button onClick={this.pause} className={cx(styles.btn, styles.btnPause)}>
                Pause!
              </button>
            )}
            {isFinished && (
              <button onClick={this.reset} className={cx(styles.btn, styles.btnReset)}>
                Again!
              </button>
            )}
          </>
        )}
      </div>
    );
  }
}

Timer.defaultProps = {
  callback: () => null,
  endingTime: 11,
};

Timer.propTypes = {
  callback: func,
  controls: bool,
  time: object,
  endingTime: number,
};

export default Timer;
