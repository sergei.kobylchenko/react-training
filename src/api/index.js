export default {
  loadList() {
    return fetch('http://localhost:8000/list')
      .then(res => res.json());
  }
}