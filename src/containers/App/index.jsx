import React, { Component } from 'react';
import './App.css';

import { Timer, List } from 'components';
import api from 'api'

class App extends Component {
  constructor(props) {
    super(props);
    this.timer = React.createRef();
  }

  state = {
    data: [],
    selected: null
  };

  getList = () => {
    return api.loadList()
      .then(res => this.setState({ data: res }));
  }

  selectItem = selected => {
    this.setState({ selected });
  };

  startTimer = () => this.timer.current.start();

  pauseTimer = () => this.timer.current.pause();

  resetTimer = () => this.timer.current.reset();

  render() {
    const { data, selected } = this.state;

    return (
      <div className="App">
        <List
          load={this.getList}
          data={data}
          clickHandler={this.selectItem}
          selected={selected}
        />

        <div>
          <div className="demo-btn">
            <button onClick={this.startTimer}>START</button>
            <button onClick={this.pauseTimer}>PAUSE</button>
            <button onClick={this.resetTimer}>RESET</button>
          </div>

          <Timer
            ref={this.timer}
            controls
            time={{ sec:12 }}
            callback={() => console.log('done')}
          />
        </div>
      </div>
    );
  }
}

export default App;
