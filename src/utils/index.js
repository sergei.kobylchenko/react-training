export default {
  getMinutes(value) {
    return toTwoDigitFormat(Math.floor(value / 60));
  },
  getSeconds(value) {
    return toTwoDigitFormat(value % 60);
  }
};

function toTwoDigitFormat(value) {
  return String(value).padStart(2, 0);
}
